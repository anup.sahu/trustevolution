import org.junit.Assert;
import org.junit.Test;

public class MachineTest {
    Machine machine = new Machine();

    @Test
    public void shouldCalculateScoreWhenBothCoop(){

        Score score =  machine.score(MoveType.COOPERATE, MoveType.COOPERATE);

        Score testScore = new Score(machine.BOTH_COOPERATES,machine.BOTH_COOPERATES);
        Assert.assertEquals(testScore, score);
    }

    @Test
    public void shouldCalculateScoreWhenBothCheat() {
        Score score =  machine.score(MoveType.CHEAT , MoveType.CHEAT);

        Score testScore = new Score(machine.BOTH_CHEATS,machine.BOTH_CHEATS);
        Assert.assertEquals(testScore, score);

    }

    @Test
    public void shouldCalculateScoreWhenCoopCheat() {
        Score score =  machine.score(MoveType.COOPERATE, MoveType.CHEAT);

        Score testScore = new Score(machine.OPPONENT_CHEATS,machine.OPPONENT_COOPERATES);
        Assert.assertEquals(testScore, score);

    }
    @Test
    public void shouldCalculateScoreWhenCheatCoop() {
        Score score =  machine.score(MoveType.CHEAT , MoveType.COOPERATE);

        Score testScore = new Score(machine.OPPONENT_COOPERATES,machine.OPPONENT_CHEATS);
        Assert.assertEquals(testScore, score);

    }

}
